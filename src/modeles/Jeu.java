package modeles;

import java.util.ArrayList;

public class Jeu {

	/**
	 * Le plateau du jeu
	 */
	private Plateau plateau;

	public Jeu() {
		this.setPlateau(new Plateau());
	}

	/**
	 * M�thode qui permet de renseigner un joueur et de l'ajouter � la liste des joueurs
	 * 
	 * @param pseudonyme Le pseudonyme du joueur
	 * @param age L'�ge du joueur
	 * @return true si le joueur a bien �t� ajout�
	 */
	public boolean renseignerJoueur(String pseudonyme, int age) {
		Joueur joueur = new Joueur(pseudonyme, age, this.getPlateau());
		boolean retour = false;
		if (this.getPlateau().getJoueurs().contains(joueur)) {
			System.out.println("Le pseudonyme " + pseudonyme + " est d�j� utilis� !");
		} else {
			this.getPlateau().ajouterJoueur(joueur);
			retour = true;
		}
		return retour;
	}
	
	/**
	 * M�thode qui vide la liste des joueurs
	 */
	public void nettoyerListeJoueur() {
		this.getPlateau().setJoueurs(new ArrayList<Joueur>());
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}
}
