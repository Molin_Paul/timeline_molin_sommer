package modeles;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Carte implements Serializable {

	/**
	 * Le nom de la carte
	 */
	private String nom;
	/**
	 * La date de l'invention de la carte
	 */
	private String date;
	/**
	 * Le nom de l'image sur la carte
	 */
	private String image;

	/**
	 * 
	 * @param nom Le nom de la carte
	 * @param date La date de l'invention sur la carte
	 * @param image Le nom de l'image sur la carte
	 */
	public Carte(String nom, String date, String image) {
		this.setNom(nom);
		this.setDate(date);
		this.setImage(image);
	}

	public String toString() {
		return this.getNom() + ", " + this.getDate();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Carte other = (Carte) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

}
