package modeles;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class Joueur implements Serializable {

	/**
	 * Le pseudonyme du joueur
	 */
	private String pseudonyme;
	/**
	 * L'�ge du joueur
	 */
	private int age;
	/**
	 * Liste des cartes du joueur
	 */
	private List<Carte> cartes;
	/**
	 * Le plateau sur lequel le joueur se trouve
	 */
	private Plateau plateau;

	/**
	 * 
	 * @param pseudonyme Le pseudonyme du joueur
	 * @param age L'�ge du joueur
	 * @param plateau Le plateau sur lequel est pr�sent le joueur
	 */
	public Joueur(String pseudonyme, int age, Plateau plateau) {
		this.setPseudonyme(pseudonyme);
		this.setAge(age);
		this.setCartes(new ArrayList<Carte>());
		this.setPlateau(plateau);
	}
	
	/**
	 * M�thode qui permet au jouer de placer une carte
	 * 
	 * @param carte La carte � placer
	 * @param place La place o� mettre la carte
	 */
	public void placerCarte(Carte carte, int place) {
		this.getCartes().remove(carte);
		this.getPlateau().getAireDeJeu().add(place, carte);
		this.getPlateau().verifierPlacement(carte, place);
	}

	public String toString() {
		String resultat = "Pseudonyme : " + this.getPseudonyme();
		resultat += "\n";
		resultat += "�ge : " + this.getAge() + " ans";
		resultat += "\n\n";
		resultat += "Liste des cartes en main : ";
		resultat += "\n";
		for (Carte carte : this.getCartes()) {
			resultat += "\n";
			resultat += carte.getNom();
		}
		resultat += "\n";
		return resultat;
	}

	public String getPseudonyme() {
		return pseudonyme;
	}

	public void setPseudonyme(String pseudonyme) {
		this.pseudonyme = pseudonyme;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<Carte> getCartes() {
		return cartes;
	}

	public void setCartes(List<Carte> cartes) {
		this.cartes = cartes;
	}

	public void ajouterCarte(Carte carte) {
		this.getCartes().add(carte);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pseudonyme == null) ? 0 : pseudonyme.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Joueur other = (Joueur) obj;
		if (pseudonyme == null) {
			if (other.pseudonyme != null)
				return false;
		} else if (!pseudonyme.equals(other.pseudonyme))
			return false;
		return true;
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

}
