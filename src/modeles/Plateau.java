package modeles;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import utils.Chargement;

@SuppressWarnings("serial")
public class Plateau implements Serializable{

	/**
	 * La liste des joueurs
	 */
	private List<Joueur> joueurs;
	/**
	 * La pile de d�fausse
	 */
	private Stack<Carte> defausse;
	/**
	 * La pile de la pioche
	 */
	private Stack<Carte> pioche;
	/**
	 * Liste des cartes pr�sentes sur l'aire de jeu
	 */
	private List<Carte> aireDeJeu;
	/**
	 * Num�ro du joueur en cours
	 */
	private int numeroJoueurEnCours;

	public Plateau() {
		this.setJoueurs(new ArrayList<Joueur>());
		this.setDefausse(new Stack<Carte>());
		this.setPioche(Chargement.cartes());
		this.setAireDeJeu(new ArrayList<Carte>());
	}

	/**
	 * M�thode qui ajoute un joueur � la liste des joueurs
	 * 
	 * @param joueur Le joueur � ajouter
	 */
	public void ajouterJoueur(Joueur joueur) {
		this.getJoueurs().add(joueur);
	}

	/**
	 * M�thode qui m�lange la pioche
	 */
	public void melangerCartes() {
		Collections.shuffle(this.getPioche());
	}

	/**
	 * M�thode qui distribue les cartes
	 */
	public void distribuerCartes() {
		int nombreDeJoueurs = this.joueurs.size();
		int nombreDeCartes = 0;
		if (nombreDeJoueurs >= 2 && nombreDeJoueurs <= 3) {
			nombreDeCartes = 6;
		}
		if (nombreDeJoueurs >= 4 && nombreDeJoueurs <= 5) {
			nombreDeCartes = 5;
		}
		if (nombreDeJoueurs >= 6 && nombreDeJoueurs <= 8) {
			nombreDeCartes = 4;
		}
		for (Joueur joueur : this.getJoueurs()) {
			for (int i = 0; i < nombreDeCartes; i++) {
				joueur.ajouterCarte(this.getPioche().pop());
			}
		}
	}

	/**
	 * M�thode qui trie les joueurs selon leur �ge et commence le jeu
	 */
	public void commencerJeu() {
		this.getAireDeJeu().add(this.getPioche().pop());
		this.getJoueurs().sort((j1, j2) -> Integer.compare(j1.getAge(), j2.getAge()));
		this.setNumeroJoueurEnCours(0);
	}

	/**
	 * M�thode qui passe le tour
	 */
	public void passerTour() {
		int numero = this.getNumeroJoueurEnCours();
		if (numero == this.getJoueurs().size() - 1) {
			this.setNumeroJoueurEnCours(0);
		} else {
			this.setNumeroJoueurEnCours(numero + 1);
		}
	}

	/**
	 * M�thode qui v�rifie si une carte est bien plac�e
	 * 
	 * @param carte La carte � v�rifier
	 * @param place La place de la carte
	 */
	public void verifierPlacement(Carte carte, int place) {
		boolean bienPlace = false;
		if (place == 0) {
			if (Integer.parseInt(carte.getDate()) <= Integer.parseInt(this.getAireDeJeu().get(place + 1).getDate())) {
				bienPlace = true;
			}
		} else if (place == this.getAireDeJeu().size() - 1) {
			if (Integer.parseInt(carte.getDate()) >= Integer.parseInt(this.getAireDeJeu().get(place - 1).getDate())) {
				bienPlace = true;
			}
		} else {
			if (Integer.parseInt(carte.getDate()) <= Integer.parseInt(this.getAireDeJeu().get(place + 1).getDate())
					&& Integer.parseInt(carte.getDate()) >= Integer
							.parseInt(this.getAireDeJeu().get(place - 1).getDate())) {
				bienPlace = true;
			}
		}

		if (!bienPlace) {
			this.getAireDeJeu().remove(place);
			this.getDefausse().add(carte);
			if(this.getPioche().size() == 0) {
				Collections.shuffle(this.getDefausse());
				this.getDefausse().forEach(c -> this.getPioche().add(c));
				this.setDefausse(new Stack<Carte>());
			}
			this.getJoueurs().get(this.getNumeroJoueurEnCours()).ajouterCarte(this.getPioche().pop());
		}
	}

	public String toString() {
		String resultat = "Liste des joueurs : ";
		resultat += "\n\n";
		//for (int i = 0; i < this.getJoueurs().size(); i++) {
		//	resultat += "Joueur " + i;
		//	resultat += "\n\n";
			resultat += this.getJoueurs().get(this.getNumeroJoueurEnCours()).toString();
			resultat += "\n";
		//}
		resultat += "Aire de jeu : ";
		resultat += "\n\n";
		int i = 0;
		for (Carte carte : this.getAireDeJeu()) {
			resultat += i;
			resultat += "\n";
			resultat += carte.toString();
			resultat += "\n";
			i++;
		}
		resultat += i;
		resultat += "\n";
		resultat += "\n";
		resultat += "D�fausse : ";
		resultat += "\n\n";
		for (Carte carte : this.getDefausse()) {
			resultat += carte.toString();
			resultat += "\n";
		}

		return resultat;
	}

	public List<Joueur> getJoueurs() {
		return joueurs;
	}

	public void setJoueurs(List<Joueur> joueurs) {
		this.joueurs = joueurs;
	}

	public Stack<Carte> getPioche() {
		return pioche;
	}

	public void setPioche(Stack<Carte> pioche) {
		this.pioche = pioche;
	}

	public Stack<Carte> getDefausse() {
		return defausse;
	}

	public void setDefausse(Stack<Carte> defausse) {
		this.defausse = defausse;
	}

	public List<Carte> getAireDeJeu() {
		return aireDeJeu;
	}

	public void setAireDeJeu(List<Carte> aireDeJeu) {
		this.aireDeJeu = aireDeJeu;
	}

	public int getNumeroJoueurEnCours() {
		return numeroJoueurEnCours;
	}

	public void setNumeroJoueurEnCours(int joueurEnCours) {
		this.numeroJoueurEnCours = joueurEnCours;
	}

}
