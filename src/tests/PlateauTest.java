package tests;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import modeles.Joueur;
import modeles.Plateau;

class PlateauTest {

	@Test
	void test_distribuer_cartes() {
		Plateau p = new Plateau();
		Joueur j = new Joueur("Jean", 10, p);
		Joueur h = new Joueur("Henri", 10, p);
		p.ajouterJoueur(j);
		p.ajouterJoueur(h);
		p.distribuerCartes();
		assertEquals("Les joueurs doivent avoir 6 cartes en main.", 6, j.getCartes().size());
	}
	
	@Test
	void test_distribuer_cartes_non() {
		Plateau p = new Plateau();
		Joueur j = new Joueur("Jean", 10, p);
		p.ajouterJoueur(j);
		p.distribuerCartes();
		assertEquals("Le joueur doit avoir aucune carte.", 0, j.getCartes().size());
	}
	
	@Test
	void test_commencerJeu_age() {
		Plateau p = new Plateau();
		Joueur j = new Joueur("Jean", 17, p);
		Joueur h = new Joueur("Henri", 10, p);
		p.ajouterJoueur(j);
		p.ajouterJoueur(h);
		p.distribuerCartes();
		p.commencerJeu();
		assertEquals("C'est Henri qui commence � jouer.", "Henri", p.getJoueurs().get(p.getNumeroJoueurEnCours()).getPseudonyme());
	}

}
