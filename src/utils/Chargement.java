package utils;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Stack;
import java.util.stream.Stream;

import modeles.Carte;

public class Chargement {

	/**
	 * M�thode qui charge les cartes
	 * @return La pile des cartes
	 */
	public static Stack<Carte> cartes() {
		//String images = "../../data/timeline/cards";
		//Files.newDirectoryStream(Paths.get(images), path -> path.toString().endsWith(".jpeg"))
			//	.forEach(System.out::println);
		
		Stack<Carte> cartes = new Stack<Carte>();
		String chemin = System.getProperty("user.dir") + "/data/timeline/timeline.csv";
		try (Stream<String> stream = Files.lines(Paths.get(chemin), Charset.defaultCharset())) {

			stream.forEach(element -> {
				String [] informations = element.split(";");
				cartes.push(new Carte(informations[0], informations[1], informations[2]));
			});
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cartes;
	}

}
