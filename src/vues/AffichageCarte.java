package vues;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import modeles.Carte;

@SuppressWarnings("serial")
public class AffichageCarte extends JPanel {

	/**
	 * La carte � afficher
	 */
	private Carte carte;
	/**
	 * L'image de face
	 */
	private BufferedImage image;
	/**
	 * L'image de dos
	 */
	private BufferedImage imageDos;

	/**
	 * 
	 * @param carte La carte � afficher
	 */
	public AffichageCarte(Carte carte) {
		this.setLayout(new FlowLayout());
		this.setPreferredSize(new Dimension(250,375)); 
		this.setCarte(carte);
		String chemin = System.getProperty("user.dir") + "/data/timeline/cards/" + this.getCarte().getImage() + ".jpeg";
		
		try {
			this.setImage(ImageIO.read(new File(chemin)));
		} catch (IOException ex) {
			System.out.println("Probl�me de chargement de l'image.");
		}
		
		chemin = System.getProperty("user.dir") + "/data/timeline/cards/" + this.getCarte().getImage() + "_date.jpeg";
		try {
			this.setImageDos(ImageIO.read(new File(chemin)));
		} catch (IOException ex) {
			System.out.println("Probl�me de chargement de l'image.");
		}
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(this.getImageDos(), 0, 0, null);
	}

	public Carte getCarte() {
		return carte;
	}

	public void setCarte(Carte carte) {
		this.carte = carte;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public BufferedImage getImageDos() {
		return imageDos;
	}

	public void setImageDos(BufferedImage imageDos) {
		this.imageDos = imageDos;
	}

}
