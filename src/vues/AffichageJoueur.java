package vues;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import modeles.Carte;
import modeles.Joueur;

@SuppressWarnings("serial")
public class AffichageJoueur extends JPanel {

	/**
	 * Le joueur � afficher
	 */
	private Joueur joueur;
	/**
	 * La carte en cours de s�lection
	 */
	private Carte carteSelectionnee;
	/**
	 * La liste de toutes les vues des cartes du joueur
	 */
	private List<AffichageCarteJoueur> cartes;

	public AffichageJoueur(Joueur joueur) {
		this.setJoueur(joueur);
		this.setCarteSelectionnee(null);
		this.setCartes(new ArrayList<AffichageCarteJoueur>());

		JLabel information = new JLabel("Au tour de " + this.getJoueur().getPseudonyme() + " de jouer !");
		information.setFont(new Font("", Font.PLAIN, 30));
		information.setBorder(new EmptyBorder(10, 10, 10, 10));
		this.add(information);
		for (Carte carte : this.getJoueur().getCartes()) {
			AffichageCarteJoueur affichage = new AffichageCarteJoueur(carte, this);
			this.getCartes().add(affichage);
			this.add(affichage);
		}
	}

	/**
	 * M�thode qui d�s�lectionne toutes les cartes du joueur
	 */
	public void deselectionner() {
		this.getCartes().forEach(AffichageCarteJoueur::deselectionner);
	}

	public Joueur getJoueur() {
		return joueur;
	}

	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}

	public Carte getCarteSelectionnee() {
		return carteSelectionnee;
	}

	public void setCarteSelectionnee(Carte carteSelectionnee) {
		this.carteSelectionnee = carteSelectionnee;
	}

	public List<AffichageCarteJoueur> getCartes() {
		return cartes;
	}

	public void setCartes(List<AffichageCarteJoueur> cartes) {
		this.cartes = cartes;
	}

}
