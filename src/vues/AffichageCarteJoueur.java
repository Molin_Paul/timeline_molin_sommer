package vues;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import modeles.Carte;

@SuppressWarnings("serial")
public class AffichageCarteJoueur extends JPanel {

	/**
	 * Oa carte � afficher
	 */
	private Carte carte;
	/**
	 * L'image de face
	 */
	private BufferedImage image;
	/**
	 * L'image de dos
	 */
	private BufferedImage imageDos;
	/**
	 * La vue du joueur
	 */
	private AffichageJoueur joueur;

	public AffichageCarteJoueur(Carte carte, AffichageJoueur joueur) {
		this.setCarte(carte);
		this.setJoueur(joueur);
		this.setBackground(Color.WHITE);
		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				joueur.setCarteSelectionnee(carte);
				joueur.deselectionner();
				selectionner();
			}
			
			  @Override
	            public void mouseEntered(MouseEvent me) {
				  setCursor(new Cursor(Cursor.HAND_CURSOR));
	            }
		});
		
		this.setPreferredSize(new Dimension(254, 379));
		String chemin = System.getProperty("user.dir") + "/data/timeline/cards/" + this.getCarte().getImage() + ".jpeg";

		try {
			this.setImage(ImageIO.read(new File(chemin)));
		} catch (IOException ex) {
			System.out.println("Probl�me de chargement de l'image.");
		}

		chemin = System.getProperty("user.dir") + "/data/timeline/cards/" + this.getCarte().getImage() + "_date.jpeg";
		try {
			this.setImageDos(ImageIO.read(new File(chemin)));
		} catch (IOException ex) {
			System.out.println("Probl�me de chargement de l'image.");
		}
	}
	
	/**
	 * S�lectionne la carte
	 */
	private void selectionner() {
		this.setBackground(Color.BLACK);
		this.revalidate();
	}
	
	/**
	 * D�s�lectionne la carte
	 */
	public void deselectionner() {
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(254, 379));
		this.revalidate();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(this.getImage(), 2, 2, null);
	}

	public Carte getCarte() {
		return carte;
	}

	public void setCarte(Carte carte) {
		this.carte = carte;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public BufferedImage getImageDos() {
		return imageDos;
	}

	public void setImageDos(BufferedImage imageDos) {
		this.imageDos = imageDos;
	}

	public AffichageJoueur getJoueur() {
		return joueur;
	}

	public void setJoueur(AffichageJoueur joueur) {
		this.joueur = joueur;
	}

}
