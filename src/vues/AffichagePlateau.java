package vues;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modeles.Plateau;

@SuppressWarnings("serial")
public class AffichagePlateau extends JPanel {

	/**
	 * Le plateau
	 */
	private Plateau plateau;
	/**
	 * La vue du joueur en cours
	 */
	private AffichageJoueur joueurEnCours;

	public AffichagePlateau(Plateau plateau) {
		this.setPlateau(plateau);
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	}

	/**
	 * M�thode qui commence la partie
	 */
	public void commencer() {
		this.removeAll();
		this.repaint();
		this.getPlateau().melangerCartes();
		this.getPlateau().distribuerCartes();
		this.getPlateau().commencerJeu();

		AffichageTable table = new AffichageTable();
		for (int i = 0; i < this.getPlateau().getAireDeJeu().size(); i++) {
			AffichageDeposerCarte depot = new AffichageDeposerCarte(this, i);
			table.add(depot);
			table.add(new AffichageCarte(this.getPlateau().getAireDeJeu().get(i)));
		}

		AffichageDeposerCarte depot = new AffichageDeposerCarte(this, this.getPlateau().getAireDeJeu().size());
		table.add(depot);
		AffichageJoueur joueur = new AffichageJoueur(
				this.getPlateau().getJoueurs().get(this.getPlateau().getNumeroJoueurEnCours()));
		this.add(table);
		this.add(joueur);
		this.setJoueurEnCours(joueur);
		this.revalidate();
	}
	
	/**
	 * M�thode qui recharge une partie enregistr�e
	 */
	public void chargerPartie() {
		this.removeAll();
		this.repaint();
		AffichageTable table = new AffichageTable();
		for (int i = 0; i < this.getPlateau().getAireDeJeu().size(); i++) {
			AffichageDeposerCarte depot = new AffichageDeposerCarte(this, i);
			table.add(depot);
			table.add(new AffichageCarte(this.getPlateau().getAireDeJeu().get(i)));
		}

		AffichageDeposerCarte depot = new AffichageDeposerCarte(this, this.getPlateau().getAireDeJeu().size());
		table.add(depot);
		AffichageJoueur joueur = new AffichageJoueur(
				this.getPlateau().getJoueurs().get(this.getPlateau().getNumeroJoueurEnCours()));
		this.add(table);
		this.add(joueur);
		this.setJoueurEnCours(joueur);
		this.revalidate();
	}

	/**
	 * M�thode qui passe le tour
	 */
	public void passerTour() {
		this.removeAll();
		this.repaint();
		if (this.getJoueurEnCours().getJoueur().getCartes().size() == 0) {
			this.add(new JLabel(this.getJoueurEnCours().getJoueur().getPseudonyme() + " a gagn� !"));
			
		} else {
			this.getPlateau().passerTour();
			AffichageTable table = new AffichageTable();
			for (int i = 0; i < this.getPlateau().getAireDeJeu().size(); i++) {
				AffichageDeposerCarte depot = new AffichageDeposerCarte(this, i);
				table.add(depot);
				table.add(new AffichageCarte(this.getPlateau().getAireDeJeu().get(i)));
			}

			AffichageDeposerCarte depot = new AffichageDeposerCarte(this, this.getPlateau().getAireDeJeu().size());
			table.add(depot);

			AffichageJoueur joueur = new AffichageJoueur(
					this.getPlateau().getJoueurs().get(this.getPlateau().getNumeroJoueurEnCours()));
			this.add(table);
			this.add(joueur);
			this.setJoueurEnCours(joueur);
		}
		this.revalidate();
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

	public AffichageJoueur getJoueurEnCours() {
		return joueurEnCours;
	}

	public void setJoueurEnCours(AffichageJoueur joueurEnCours) {
		this.joueurEnCours = joueurEnCours;
	}

}
