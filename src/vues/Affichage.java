package vues;

import java.awt.FileDialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import modeles.Jeu;
import modeles.Plateau;

@SuppressWarnings("serial")
public class Affichage extends JFrame {

	/**
	 * Le jeu
	 */
	private Jeu jeu;
	/**
	 * La vue du plateau
	 */
	private AffichagePlateau plateau;

	public Affichage() {
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setTitle("Timeline");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		String chemin = System.getProperty("user.dir") + "/data/logo.png";
		this.setIconImage(new ImageIcon(chemin).getImage());
		this.setResizable(true);

		this.setJeu(new Jeu());
		this.commencer();
		this.setVisible(true);
	}

	/**
	 * M�thode qui pr�pare le jeu et assemble la fen�tre
	 */
	private void commencer() {

		Jeu jeu = this.getJeu();

		JPanel panel_racine = new JPanel();
		this.setContentPane(panel_racine);

		JMenuBar menu_bar = new JMenuBar();
		JMenuItem menu_1 = new JMenuItem("Charger une partie");

		JMenuItem menu_2 = new JMenuItem("Sauvegarder la partie");

		menu_2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				FileDialog dialog = new FileDialog((Frame) null, "Sauvegarder la partie");
				dialog.setMode(FileDialog.LOAD);
				dialog.setVisible(true);
				String file = dialog.getFile();
				if (file != null) {
					ObjectOutputStream oos = null;
					try {
						final FileOutputStream fichier = new FileOutputStream(file);
						oos = new ObjectOutputStream(fichier);
						oos.writeObject(getJeu().getPlateau());
					} catch (final java.io.IOException ie) {
						ie.printStackTrace();
					} finally {
						try {
							if (oos != null) {
								oos.flush();
								oos.close();
							}
						} catch (final IOException ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		});

		JMenu menu = new JMenu("Fichier");
		menu.add(menu_1);
		menu.add(menu_2);

		menu_2.setEnabled(false);
		menu_bar.add(menu);
		this.setJMenuBar(menu_bar);

		JPanel panel_1 = new JPanel();
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.PAGE_AXIS));

		JLabel label_1 = new JLabel("Choisissez un nombre de joueurs");
		label_1.setFont(new Font("", Font.PLAIN, 30));
		label_1.setBorder(new EmptyBorder(10, 10, 10, 10));
		JTextField champs_1 = new JTextField("2");

		JButton bouton_1 = new JButton("Valider");

		panel_1.add(label_1);
		panel_1.add(champs_1);
		panel_1.add(bouton_1);

		JPanel panel_2 = new JPanel();
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.PAGE_AXIS));
		JButton bouton_2 = new JButton("Valider");

		panel_2.setVisible(false);

		AffichagePlateau plateau = new AffichagePlateau(jeu.getPlateau());
		this.setPlateau(plateau);
		plateau.setVisible(false);

		bouton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String pseudonyme = null;
				int age = -1;
				int selection = 0;
				boolean retour = true;
				for (int i = 0; i < panel_2.getComponents().length; i++) {
					if (panel_2.getComponents()[i] instanceof JTextField) {
						// Si c'est un champs d�signant le pseudonyme
						if (selection == 0) {
							selection = 1;
							pseudonyme = ((JTextField) panel_2.getComponents()[i]).getText();

							// Si c'est un champs d�signant l'�ge
						} else {
							selection = 0;
							try {
								
								age = Integer.parseInt(((JTextField) panel_2.getComponents()[i]).getText());
								boolean insertion = jeu.renseignerJoueur(pseudonyme, age);
								if (!insertion) {
									retour = insertion;
								}
							} catch (NumberFormatException nfe) {
								System.out.println("�ge invalide !");
								retour = false;
							}
						}
					}
				}

				if (retour) {
					System.out.println("D�but du jeu !");
					panel_1.setVisible(false);
					panel_2.setVisible(false);
					plateau.setVisible(true);

					plateau.commencer();
					menu_2.setEnabled(true);
				} else {
					jeu.nettoyerListeJoueur();
					System.out.println("Erreur !");
				}
			}
		});

		bouton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int nombre = 0;
				try {
					nombre = Integer.parseInt(champs_1.getText());
					if (nombre >= 2 && nombre <= 8) {
						panel_1.setVisible(false);
						for (int i = 0; i < nombre; i++) {
							JLabel label_4 = new JLabel("Joueur" + (i + 1));
							label_4.setFont(new Font("", Font.PLAIN, 30));
							label_4.setBorder(new EmptyBorder(10, 10, 10, 10));
							panel_2.add(label_4);

							JLabel label_2 = new JLabel("Pseudonyme");
							label_2.setFont(new Font("", Font.PLAIN, 20));
							panel_2.add(label_2);
							panel_2.add(new JTextField());

							JLabel label_3 = new JLabel("�ge");
							label_3.setFont(new Font("", Font.PLAIN, 20));
							panel_2.add(label_3);
							panel_2.add(new JTextField());
						}
						panel_2.add(bouton_2);
						panel_2.setVisible(true);
						panel_racine.revalidate();

					} else {
						System.out.println("Nombre de joueurs incorrect !");
					}
				} catch (NumberFormatException nfe) {
					System.out.println("Nombre invalide !");
				}

			}
		});

		menu_1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				FileDialog dialog = new FileDialog((Frame) null, "Charger une partie");
				dialog.setMode(FileDialog.LOAD);
				dialog.setVisible(true);
				String file = dialog.getFile();
				if (file != null) {
					ObjectInputStream ois = null;
					try {
						final FileInputStream fichier = new FileInputStream(file);
						ois = new ObjectInputStream(fichier);
						Plateau p = (Plateau) ois.readObject();
						getPlateau().setPlateau(p);
						panel_1.setVisible(false);
						panel_2.setVisible(false);
						plateau.setVisible(true);
						menu_2.setEnabled(true);
						getPlateau().chargerPartie();
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} finally {
						try {
							if (ois != null) {
								ois.close();
							}
						} catch (final IOException ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		});

		panel_racine.add(panel_1);
		panel_racine.add(panel_2);
		panel_racine.add(plateau);
	}

	public Jeu getJeu() {
		return jeu;
	}

	public void setJeu(Jeu jeu) {
		this.jeu = jeu;
	}

	public AffichagePlateau getPlateau() {
		return plateau;
	}

	public void setPlateau(AffichagePlateau plateau) {
		this.plateau = plateau;
	}

}
