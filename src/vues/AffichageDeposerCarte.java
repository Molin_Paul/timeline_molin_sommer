package vues;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class AffichageDeposerCarte extends JPanel {

	/**
	 * La vue du plateau
	 */
	private AffichagePlateau plateau;
	/**
	 * La place du d�p�t
	 */
	private int place;

	public AffichageDeposerCarte(AffichagePlateau plateau, int place) {
		this.setLayout(new FlowLayout());
		this.setPlateau(plateau);
		this.setPlace(place);
		this.setPreferredSize(new Dimension(100, 100));
		this.setBackground(Color.LIGHT_GRAY);

		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				if (plateau.getJoueurEnCours().getCarteSelectionnee() != null) {
					plateau.getJoueurEnCours().getJoueur()
							.placerCarte(plateau.getJoueurEnCours().getCarteSelectionnee(), place);
					plateau.passerTour();
				}
			}
			
			 @Override
	            public void mouseEntered(MouseEvent me) {
				  setCursor(new Cursor(Cursor.HAND_CURSOR));
	            }
		});
	}

	public AffichagePlateau getPlateau() {
		return plateau;
	}

	public void setPlateau(AffichagePlateau plateau) {
		this.plateau = plateau;
	}

	public int getPlace() {
		return place;
	}

	public void setPlace(int place) {
		this.place = place;
	}

}
